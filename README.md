# my_cowsay

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### 使用方式
```
import cowsay from "cowsay2";

cowsay.say('牛转乾坤')
cowsay.say('牛转乾坤', { mode: 'paranoid' })

进阶用法
const whale = require('cowsay2/cows/whale.js');
this.sow1 = cowsay.say('happy new year', { cow: whale })
```

### 效果
```
 __________
< 牛转乾坤 >
 ----------
        \   ^__^
         \  (@@)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
                
 ________________
< happy new year >
 ----------------
   \
    \
     \
                '-.
      .---._     \ .--'
    /       `-..__)  ,-'
   |    0           /
    --.__,   .__.,`
     `-.___'._\_.'

```


